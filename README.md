# README #

This code demos for using Selenium WebDriver to do the automation testing on thhe landing page of http://kate.recruitkits.com/.

### Technologies ###

* Selenium Web driver
* TestNG
* Hybid framework
* Programing language: Java

### Flow of the code ###

* Read data from excel file to an String[][] array, put to Data provider: each column data (means data of all test step of each test case) is saved as a List.
* Factory get data for each run time returned by Data provider.
* Each runtime, run a list of methods of a Test cases.
* After each test method, a screen shot will be taken
* After each test method, test result of that test method will be saved to a List<String[]>.
* After the test suite, test result in the List<String[]> will be written to a excel file.
