package pageObjects;

import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.*;

public class LandingPage extends BaseTest {
		
	@Factory(dataProvider = "getDataForInstances")
	public LandingPage(List<String> data) {
		super(data);
	}

	/*
	 * TEST METHODs
	 */
	@Test(description = "1 - Launches the Landing page")
	public void launchSite() {
		String title = getDataText("launchSite");

		if (!title.equalsIgnoreCase("not required")) {
			driver.get("http://kate.recruitkits.com");
			waitForPageLoaded(5);
			
			if (driver.getTitle().equalsIgnoreCase(title)) {
				Reporter.log("PASSED!");
				System.out.println("PASSED!");
			} else {
				Reporter.log("FAILED!");
				System.err.println("FAILED! - Actual Result is: " + driver.getTitle() + ". \n Expected: " + title);

				Assert.fail("FAILED! - Actual Result is: " + driver.getTitle() + ". \n Expected: " + title);
			}
		} else {
			System.out.println("IGNORE method launchSite");
		}
	}

	@Test(description = "2 - Verify search job link", dependsOnMethods = "launchSite")
	public void verifySearchJobLink() {
		System.out.println("------verifySearchJobLink - This is test verifySearchJobLink.");
		if (!getDataText("verifySearchJobLink").equalsIgnoreCase("not required")) {
			String expectedResult = getDataText("verifySearchJobLink");
			waitUntilExist(10, getObjectBy("XPATH.LandingPage.link_searchJob"));
			WebElement searchLink = getElement("XPATH.LandingPage.link_searchJob");
			waitUntilVisible(10, searchLink);
			String actualResult = searchLink.getAttribute("href");

			if (actualResult.equalsIgnoreCase(expectedResult)) {				
				Reporter.log("PASSED!");
				System.out.println("PASSED!");
			} else {
				Reporter.log("FAILED!");
				System.out.println("FAILED! - Actual Result is: " + actualResult + ". \n Expected: " + expectedResult);

				Assert.fail("FAILED! - Actual Result is: " + actualResult + ". \n Expected: " + expectedResult);
			}
		} else {
			System.out.println("IGNORE - This is test verifySearchJobLink.");
		}

	}

	@Test(description = "3 - Input search info", dependsOnMethods = "verifySearchJobLink")
	public void inputSearchInfo() {
		if (!getDataText("inputSearchInfo").equalsIgnoreCase("not required")) {
			String searchCriteria = getDataText("inputSearchInfo");
			try {
				waitUntilExist(10, getObjectBy( "XPATH.LandingPage.input_search"));
				WebElement searchTxt = getElement("XPATH.LandingPage.input_search");
				searchTxt.sendKeys(searchCriteria);

				Reporter.log("PASSED!");
				System.out.println("PASSED!");
			} catch (Exception e) {
				Reporter.log("FAILED!");
				System.err.println("FAILED! Message is: " + e.getMessage());

				Assert.fail("Failed for inputing search criteria. Error message is: " + e.getMessage());
			}
		} else {
			System.out.println("IGNORE - This is test inputSearchInfo.");
		}
	}	

	@Test(description = "3 - Select Location filter", dependsOnMethods = "inputSearchInfo")
	public void selectLocation() {
		if (!getDataText("selectLocation").equalsIgnoreCase("not required")) {
			String locationValue = getDataText("selectLocation");
			try {
				waitUntilExist(10, getObjectBy("XPATH.LandingPage.dropdownbox_location"));
				WebElement locationDropdown = getElement("XPATH.LandingPage.dropdownbox_location");
				Actions action = new Actions(driver);
				action.moveToElement(locationDropdown).sendKeys(locationValue);
				
				Reporter.log("PASSED!");
				System.out.println("PASSED!");
			} catch (Exception e) {
				Reporter.log("FAILED!");
				System.err.println("FAILED! Message is: " + e.getMessage());

				Assert.fail("Failed for select a location. Error message is: " + e.getMessage());
			}
		} else {
			System.out.println("IGNORE - This is test selectLocation.");
		}
	}

	@Test(description = "4 - Click search button", dependsOnMethods = "inputSearchInfo")
	public void clickSearchButton() {
		if (!getDataText("clickSearchButton").equalsIgnoreCase("not required")) {
			try {
				WebElement searchButton = getElement("XPATH.LandingPage.button_search");
				waitUntilClickable(5, searchButton);
				searchButton.click();

				Reporter.log("PASSED!");
				System.out.println("PASSED!");
			} catch (Exception e) {
				Reporter.log("FAILED!");
				System.err.println("FAILED! Message is: " + e.getMessage());

				Assert.fail("Failed for clicking the search button. Error message is: " + e.getMessage());
			}
		} else {
			System.out.println("IGNORE - This is test clickSearchButton.");
		}

	}

	@Test(description = "5 - Verify search Result", dependsOnMethods = "clickSearchButton")
	public void verifySearchResult() {
		System.out.println("------verifySearchResult - This is test verifySearchResult.");
		if (!getDataText("verifySearchResult").equalsIgnoreCase("not required")) {
			waitForPageLoaded(5);
			String title = getDataText("verifySearchResult");
			if (driver.getTitle().equalsIgnoreCase(title)) {
				Reporter.log("PASSED!");
				System.out.println("PASSED!");
			} else {
				Reporter.log("FAILED!");
				System.err.println("FAILED!");

				Assert.fail("FAILED! - Actual Result is: " + driver.getTitle() + ". \n Expected: " + title);
			}
		} else {
			System.out.println("IGNORE - This is test verifySearchResult.");
		}

	}

}
