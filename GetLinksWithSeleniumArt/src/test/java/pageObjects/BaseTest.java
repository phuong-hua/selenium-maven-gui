package pageObjects;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
//import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Parameters;

public class BaseTest extends CommonMethods {
	private List<String> data;
	private static List<String> keywordData = new ArrayList<>();
	private static String testName;
	private String driverPath = System.getProperty("user.dir") + "\\src\\test\\java\\drivers\\";
	private static List<String[]> testOuputResultList = new ArrayList<>();

	public WebDriver driver;
	public static Properties prop;

	/*
	 * COMMON ACTIONS for each Object Page: START
	 */
	@Factory(dataProvider = "getDataForInstances")
	public BaseTest(List<String> data) {
		this.data = data;
	}

	/*
	 * GET DATA FROM EXCEL DATA FILE: - Each column data is added to a List<String>
	 * - Column of keywords is added to specific List<String>
	 */
	@DataProvider(name = "getDataForInstances")
	public static Object[][] getDataForInstances(ITestContext iContext) {
		System.out.println("Start getDataForInstance");
		String excelTestDataFile = iContext.getCurrentXmlTest().getParameter("excelTestDataFile");
		String[][] dataBe4 = getExcelData(excelTestDataFile);

		// Array of columns data which is added to a list for each data.
		// Only get data of Test cases, the first column is Keyword info then no need.
		Object[][] arrayObjects = new Object[totalCols - 1][1];

		// Get all data of test cases to an array of List<String>
		for (int col = 1; col < totalCols; col++) {
			List<String> colData = new ArrayList<>();
			for (int row = 0; row < totalRows; row++) {
				// Add data of each cell in a column to a List<String>
				colData.add(dataBe4[row][col]);
			}
			// Add all column data to a List array. (Which is have only 1 column)
			arrayObjects[col - 1][0] = colData;
		}

		// Add data of keywords(First column in excel file) to a static List<String> for
		// using in all run times.
		for (int row = 0; row < totalRows; row++) {
			keywordData.add(dataBe4[row][0]);
		}

		return arrayObjects;
	}

	/*
	 * START SUITE: create a Runtime folder to save test screen shots
	 */
	@BeforeSuite
	@Parameters("properties")
	public void beforeSuite(String propertyFile) throws IOException {
		// Create a time run folder.
		createRuntimeFolder("LandingPage");
		// Loading properties.
		prop = getProperties(propertyFile);
	}

	/*
	 * START CLASS: equivalents to start to run a test case. - Initialize a Chrome
	 * driver. - Get Locators from properties.
	 */
	@BeforeClass
	@Parameters("browser")
	public void beforeClass(String browser) throws Exception {
		System.out.println("*********************************");
		System.out.print("Running Test case - ");
		System.out.println(data.get(0));
		testName = data.get(0);
		System.out.println("*********************************");

		if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", driverPath + "geckodriver.exe");
			driver = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver_2-3-9.exe");
			driver = new ChromeDriver();
		} else {
			throw new Exception("Cannot start a driver");
		}
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		setDriver(driver);
	}

	// Take screenshot after each method
	@AfterMethod
	@Parameters("browser")
	public void afterMethod(ITestResult iResult, Method method, String browser) {
		// Take screen shot.
		String screenShotPath = takeScreenShot(driver, browser, testName, method.getName());

		// Save a string[] of test method to Test result list.
		String failureError = getTestResult(iResult) == "PASSED" ? "" : iResult.getThrowable().getMessage();
		String[] testMethodOuput = { testName, browser, method.getName(), getTestResult(iResult), failureError,
				screenShotPath };
		testOuputResultList.add(testMethodOuput);
	}

	@AfterClass
	@Parameters("browser")
	public void afterClass(String browser) {
		driver.close();
		addTestCaseOuputToList(testOuputResultList, testName, browser);
	}

	@AfterSuite
	public void afterSuite() throws IOException {
		String fileOutputPath = runtimeFolder + "\\";
		String fileOutputName = "TestResult_LandingPage.xlsx";
		writeExcel(fileOutputPath, fileOutputName, testOuputResultList);
	}

	/*
	 * COMMON ACTIONS for each Object Page: END
	 */

	/*
	 * METHODS
	 */

	public void addTestCaseOuputToList(List<String[]> allTestResultLists, String currentTestName, String platform) {
		boolean flag = true;
		String testcaseResult = "PASSED";// PASSED, FAILED
		for (String[] testMethodOutput : allTestResultLists) {
			if (testMethodOutput[0] == currentTestName && testMethodOutput[1] == platform) {
				if (testMethodOutput[3] == "FAILED") {
					flag = false;
					break;
				}
			}
		}

		if (flag == false) {
			testcaseResult = "FAILED";
		}

		String[] testcaseOutput = { "TEST RESULT: " + testName, platform, testcaseResult };
		allTestResultLists.add(testcaseOutput);
	}

	// Get data text of a cell based on keyword - the first cell value - in row.
	public String getDataText(String keywordName) {
		String result = "";
		for (int i = 0; i < keywordData.size(); i++) {
			if (keywordName.equalsIgnoreCase(keywordData.get(i))) {
				result = data.get(i);
				break;
			}
		}
		return result;
	}

	// Get test result string of a iTestResult
	public String getTestResult(ITestResult iResult) {
		String result = "";
		if (iResult.getStatus() == ITestResult.FAILURE)
			result = "FAILED";
		else if (iResult.getStatus() == ITestResult.SKIP)
			result = "SKIPPED";
		else if (iResult.getStatus() == ITestResult.SUCCESS)
			result = "PASSED";
		return result;
	}

	/*
	 * ====================================================================
	 * PROPERTIES METHODS
	 * ====================================================================
	 */


	// Get a object property from properties list
	public static By getObjectBy(String objectName) {
		String objectType = objectName.substring(0, objectName.indexOf("."));
		By objectBy = null;
		switch (objectType.toUpperCase()) {
		case "XPATH":
			objectBy = By.xpath(prop.getProperty(objectName));
			break;
		case "CLASSNAME":
			objectBy = By.className(prop.getProperty(objectName));
			break;
		case "NAME":
			objectBy = By.name(prop.getProperty(objectName));
			break;
		case "CSS":
			objectBy = By.cssSelector(prop.getProperty(objectName));
			break;
		case "LINK":
			objectBy = By.linkText(prop.getProperty(objectName));
			break;
		case "PARTIALLINK":
			objectBy = By.partialLinkText(prop.getProperty(objectName));
			break;
		default:
			break;
		}
		return objectBy;
	}

	// Get a element of an object property
	public WebElement getElement(String objectName) {
		By objectBy = getObjectBy(objectName);
		return driver.findElement(objectBy);
	}

	// Get multi elements of an object property
	public List<WebElement> getElements(String objectName) {
		By objectBy = getObjectBy(objectName);
		return driver.findElements(objectBy);
	}

}
