package pageObjects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.apache.commons.io.FileUtils;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.formula.functions.Column;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class CommonMethods {

	private WebDriver driver = null;

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	/*
	 * ====================================================================
	 * PROPERTIES METHODS
	 * ====================================================================
	 */

	public static int currentColNum = 0;
	public static int totalRows = 0; // ~ with the number of Keywords.
	public static int totalCols = 0; // ~ with the number of Columns.
	public static Workbook dataWorkbook = null;
	public static Sheet dataSheet = null;
	private static XSSFCell cell;
	static String testInputFilePath = System.getProperty("user.dir") + "\\src\\test\\java\\data\\";

	/*
	 * ====================================================================
	 * PROPERTIES METHODS
	 * ====================================================================
	 */

	// Read properties file
	public static Properties getProperties(String fileName) throws IOException {
		File file = new File(System.getProperty("user.dir") + "/src/test/java/properties/" + fileName);
		FileInputStream inputStream = new FileInputStream(file);

		Properties p = new Properties();
		p.load(inputStream);

		return p;
	}

	// Get a object property from properties list
	public static By getObjectBy(Properties p, String objectName) {
		String objectType = objectName.substring(0, objectName.indexOf("."));
		By objectBy = null;
		switch (objectType.toUpperCase()) {
		case "XPATH":
			objectBy = By.xpath(p.getProperty(objectName));
			break;
		case "CLASSNAME":
			objectBy = By.className(p.getProperty(objectName));
			break;
		case "NAME":
			objectBy = By.name(p.getProperty(objectName));
			break;
		case "CSS":
			objectBy = By.cssSelector(p.getProperty(objectName));
			break;
		case "LINK":
			objectBy = By.linkText(p.getProperty(objectName));
			break;
		case "PARTIALLINK":
			objectBy = By.partialLinkText(p.getProperty(objectName));
			break;
		default:
			break;
		}
		return objectBy;
	}

	// Get a element of an object property
	public WebElement getElement(Properties p, String objectName) {
		By objectBy = getObjectBy(p, objectName);
		return driver.findElement(objectBy);
	}

	// Get multi elements of an object property
	public List<WebElement> getElements(Properties p, String objectName) {
		By objectBy = getObjectBy(p, objectName);
		return driver.findElements(objectBy);
	}
	
	public void focusToElement(WebElement element) {
		Actions action = new Actions(driver);
		action.moveToElement(element);
	}

	/*
	 * ============================================================================
	 * EXCEL METHODS
	 * ============================================================================
	 */

	// Read an excel file
	public static Sheet readExcel(String fileName) throws IOException {
		File file = new File(testInputFilePath + fileName);
		FileInputStream inputStream = new FileInputStream(file);

		dataWorkbook = null;
		String fileNameExtension = fileName.substring(fileName.indexOf("."));
		if (fileNameExtension.equalsIgnoreCase(".xlsx")) {
			dataWorkbook = new XSSFWorkbook(inputStream);
		} else if (fileNameExtension.equalsIgnoreCase(".xls")) {
			dataWorkbook = new HSSFWorkbook(inputStream);
		}

		Sheet dataSheet = dataWorkbook.getSheetAt(0);
		inputStream.close();

		return dataSheet;
	}

	// Get excel data and save to a 2nd directions array.
	public static String[][] getExcelData(String fileName) {

		String[][] arrayExcelData = null;
		try {
			dataSheet = readExcel(fileName);
			totalRows = dataSheet.getPhysicalNumberOfRows();
			totalCols = dataSheet.getRow(0).getPhysicalNumberOfCells();

			arrayExcelData = new String[totalRows][totalCols];
			int startRow = 0;
			int startCol = 0;

			for (int i = startRow; i < totalRows; i++) {
				for (int j = startCol; j < totalCols; j++) {
					arrayExcelData[i][j] = getCellData(i, j);
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return arrayExcelData;
	}

	// Get cell data
	public static String getCellData(int rowNum, int colNum) {
		String cellData = "";
		try {
			cell = (XSSFCell) dataSheet.getRow(rowNum).getCell(colNum);
			switch (cell.getCellType()) {
			case BOOLEAN:
				cellData = cell.getRawValue();
				break;
			case NUMERIC:
				cellData = cell.getRawValue();
				break;
			case STRING:
				cellData = cell.getStringCellValue();
				break;
			case ERROR:
				cellData = "";
			default:
				break;
			}

		} catch (Exception e) {
			cellData = "";
		}
		return cellData;
	}

	// Write test result to Excel file
	public static void writeExcel(String filePath, String fileName, List<String[]> dataToWrite) throws IOException {
		Workbook outputWorkbook = new XSSFWorkbook();
		Sheet outputSheet = outputWorkbook.createSheet("Test Result");
		try {
			/*
			 * CreationHelper helps us create instances of various things like DataFormat,
			 * Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way
			 */
			CreationHelper createHelper = outputWorkbook.getCreationHelper();

			// Create Font types which will be set to cells later.
			Font headerFont = outputWorkbook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());

			Font testcaseFont = outputWorkbook.createFont();
			testcaseFont.setBold(true);
			testcaseFont.setColor(IndexedColors.BLUE.getIndex());

			Font failedFont = outputWorkbook.createFont();
			failedFont.setBold(true);
			failedFont.setColor(IndexedColors.RED.getIndex());

			Font passedFont = outputWorkbook.createFont();
			passedFont.setColor(IndexedColors.GREEN.getIndex());

			// Create CellStyles which will be set to cells laters.
			CellStyle headerCellStyle = outputWorkbook.createCellStyle();
			headerCellStyle.setFont(headerFont);

			CellStyle testcaseStyle = outputWorkbook.createCellStyle();
			testcaseStyle.setFont(testcaseFont);

			CellStyle failedStyle = outputWorkbook.createCellStyle();
			failedStyle.setFont(failedFont);

			CellStyle passedStyle = outputWorkbook.createCellStyle();
			passedStyle.setFont(passedFont);

			// Create header fields of the sheet in the first row
			String[] headerOutputField = { "Test Case", "Platform", "Test Method", "Result", "Test info",
					"Screenshot info" };
			Row headerRow = outputSheet.createRow(0);
			for (int i = 0; i < headerOutputField.length; i++) {
				Cell cell = headerRow.createCell(i);
				cell.setCellValue(headerOutputField[i]);
				cell.setCellStyle(headerCellStyle);
			}

			// Populate data to the worksheet from the second row.
			int rowNum = 1;
			for (String[] testResult : dataToWrite) {
				Row newRow = outputSheet.createRow(rowNum++);
				for (int i = 0; i < testResult.length; i++) {
					Cell newCell = newRow.createCell(i);
					if (i == 5) {
						newCell.setCellValue("Click here to see the screenshot");
						Hyperlink link = createHelper.createHyperlink(HyperlinkType.FILE);
						String URLLink = testResult[i].replace("\\", "/");
						link.setAddress(URLLink);
						newCell.setHyperlink(link);
					} else {
						newCell.setCellValue(testResult[i]);
					}
				}

				// Format the row if that row include result of TEST CASE
				if (testResult[0].toLowerCase().contains("TEST RESULT: ".toLowerCase())) {
					newRow.getCell(0).setCellStyle(testcaseStyle);
					if (testResult[2] == "PASSED")
						newRow.getCell(2).setCellStyle(passedStyle);
					else if (testResult[2] == "FAILED")
						newRow.getCell(2).setCellStyle(failedStyle);
				}
			}

			// Write data to excel file
			File fileOutput = new File(filePath + fileName);
			FileOutputStream outputStream = new FileOutputStream(fileOutput);
			outputWorkbook.write(outputStream);
			outputStream.close();
		} catch (Exception e) {
			System.err.println("Exception message: " + e.getCause().getMessage());
		}
	}

	/*
	 * ============================================================================
	 * SCREEN SHOT METHODS
	 * ============================================================================
	 */

	public static File runtimeFolder;

	// Step1: SUITE LEVEL - Create a runtime folder for running time of the object
	// Page.
	public static void createRuntimeFolder(String objectPage) {

		String screenshotPagePath = System.getProperty("user.dir") + "\\test-result\\screenShot\\" + objectPage + "\\";
		try {
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
			runtimeFolder = new File(screenshotPagePath + timeStamp);
			runtimeFolder.mkdir();

		} catch (Exception e) {
			System.out.println("Exception message: " + e.getCause().getMessage());
		}
	}

	// Step2: TEST LEVEL - Create screenshot and save to test case folder based on
	// testName and platform info.
	public static String takeScreenShot(WebDriver driver, String platform, String testName, String imageFileName) {

		TakesScreenshot scrShot = ((TakesScreenshot) driver);
		File scrFile = scrShot.getScreenshotAs(OutputType.FILE);
		String runtimePath = runtimeFolder.getAbsolutePath();
		String testScreenFileNamePath = runtimePath + "\\" + platform + "\\" + testName + "\\" + platform + "_"
				+ imageFileName + ".jpg";

		File outputFile = new File(testScreenFileNamePath);
		try {
			FileUtils.copyFile(scrFile, outputFile);
		} catch (IOException ex) {
			System.out.println(Level.SEVERE + " Failed to save screen shot to " + outputFile);
		}
		return testScreenFileNamePath;
	}

	/*
	 * ===========================================================================
	 * WAIT METHODS
	 * ===========================================================================
	 */
	WebDriverWait explicitWait;

	public void waitForPageLoaded(long waitTime) {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			// Implement the abstract apply method of ExpectedCondition class to get page
			// state then check whether it is ready.
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
						.equals("complete");
			}
		};

		try {
			explicitWait = new WebDriverWait(driver, waitTime);
			explicitWait.until(expectation);
		} catch (Throwable error) {
			System.err.println("Timeout waiting for loading page.");
		}
	}

	public void waitUntilClickable(long waitTime, WebElement webElement) {
		try {
			explicitWait = new WebDriverWait(driver, waitTime);
			explicitWait.until(ExpectedConditions.elementToBeClickable(webElement));
		} catch (Throwable error) {
			System.err.println("Timeout waiting for element clickable.");
		}
	}

	public void waitUntilVisible(long waitTime, WebElement webElement) {
		try {
			explicitWait = new WebDriverWait(driver, waitTime);
			explicitWait.until(ExpectedConditions.visibilityOf(webElement));
		} catch (Throwable error) {
			System.err.println("Timeout waiting for element visible.");
		}
	}

	public void waitUntilInvisible(long waitTime, WebElement webElement) {
		try {
			explicitWait = new WebDriverWait(driver, waitTime);
			explicitWait.until(ExpectedConditions.invisibilityOf(webElement));
		} catch (Throwable error) {
			System.err.println("Timeout waiting for element invisible.");
		}
	}

	public void waitUntilExist(long waitTime, By waitLocator) {
		try {
			explicitWait = new WebDriverWait(driver, waitTime);
			explicitWait.until(ExpectedConditions.presenceOfElementLocated(waitLocator));
		} catch (Throwable error) {
			System.err.println("Timeout waiting for element exists.");
		}
	}

	/*
	 * ====================================================================
	 * JAVASCRIPTS METHODS
	 * ====================================================================
	 */
	public Object executeJavascript(WebDriver argDriver, String javascript) {
		JavascriptExecutor js = (JavascriptExecutor) argDriver;
		return js.executeScript(javascript).toString();

	}

}
